let initialState = {
  pokemonData: [],
  pokemonError: {},
  isFetching: false
};

export const reducer = (state = initialState, action) => {
  switch(action.type) {
    case 'FETCH_POKEMON_DATA':
      return Object.assign({}, state, {
        isFetching: true
      });
    case 'FETCH_POKEMON_DATA_SUCCESS':
      return Object.assign({}, state, {
        pokemonData: action.data,
        isFetching: false
      });
    case 'FETCH_POKEMON_DATA_FAILURE':
      return Object.assign({}, state, {
        pokemonError: action.error,
        isFetching: false
      });
    case 'FETCH_POKEMON_TYPES':
      return Object.assign({}, state, {
        isFetching: true
      });
    case 'FETCH_POKEMON_TYPES_SUCCESS':
      return Object.assign({}, state, {
        pokemonTypes: action.data,
        isFetching: false
      });
    case 'FETCH_POKEMON_TYPES_FAILURE':
      return Object.assign({}, state, {
        pokemonError: action.error,
        isFetching: false
      });
    default:
      return state;
  }
}

export default reducer;
