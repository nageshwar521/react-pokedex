import React, { Component } from 'react';
import Ionicon from 'react-ionicons';
import { Button } from 'reactstrap';
import cn from 'classnames';

const iconStyle = {
  position: "relative",
  top: "3px"
};

const getFontSize = (fontSize, onlyIcon) => {
  return fontSize ? fontSize : onlyIcon ? "20px" : "18px";
}

class ReactIconButton extends Component {
  constructor() {
    super();
    this.state = {
      color: ""
    };
  }
  componentWillMount() {
    if (this.props.color) {
      this.setState({
        color: this.props.color
      });
    }
  }
  onMouseOver = () => {
    if (this.props.hoverColor) {
      this.setState({
        color: this.props.hoverColor
      });
    }
  }
  onMouseOut = () => {
    if (this.props.color) {
      this.setState({
        color: this.props.color
      });
    }
  }
  onIconClick = (ev) => {
    console.log("onIconClick");
    const { onIconClick } = this.props;
    if (onIconClick) {
      onIconClick(ev);
    }
  }
  render() {
    const {
      ios,
      md,
      text,
      fontSize,
      onlyIcon,
      fullWidth,
      style,
      extraIconClass,
      btnRoundClass,
      btnPaddingClass,
      btnPositionClass
    } = this.props;
    const { color } = this.state;
    const icon = ios ? `ios-${ios}` : `md-${md}`;
    const btnClass = cn("btn btn-outline-light border-0 text-left",
                          btnRoundClass,
                          btnPaddingClass,
                          btnPositionClass,
                          fullWidth ? "btn-block" : ""
                        );
    const iconClass = cn(extraIconClass);
    return (
      <Button
        className={btnClass}
        onClick={this.onIconClick}
        onMouseOver={this.onMouseOver}
        onMouseOut={this.onMouseOut}
        style={style}
      >
        <Ionicon
          className={iconClass}
          style={iconStyle}
          icon={icon}
          color={color}
          fontSize={getFontSize(fontSize, onlyIcon)} />
        <span>{text}</span>
      </Button>
    );
  }
};

ReactIconButton.defaultProps = {
  btnPositionClass: "",
  btnRoundClass: "rounded-0",
  btnPaddingClass: "",
  color: "white",
  hoverColor: "black",
  extraIconClass: ""
}

export default ReactIconButton;
