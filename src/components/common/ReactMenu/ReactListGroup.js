import React, { Component } from 'react';
import './styles.css';
import {
  ListGroup,
  ListGroupItem,
  Card,
  CardHeader,
  CardBody,
  Button,
  ButtonGroup,
  InputGroup,
  InputGroupAddon,
  NavLink
} from 'reactstrap';
import ReactIconButton from '../ReactIconButton';

const cardStyle = (onlyIcon) => {
  return {
    width: onlyIcon ? "46px" : "200px",
    paddingTop: "55px"
  };
};

export default class ReactListGroup extends Component {
  generateList = () => {
    const { items = [], onlyIcon, fullWidth = false } = this.props;
    return items.map((item, index) => {
      const { ios, md } = item;
      return <ListGroupItem key={index} className="rounded-0 p-0">
        <ReactIconButton
          ios={ios}
          md={md}
          onlyIcon={onlyIcon}
          text={item.text}
          fullWidth={fullWidth}
          extraIconClass="mr-1"
        />
      </ListGroupItem>;
    });
  };
  generateIcons = () => {
    const { items = [], onlyIcon, fullWidth } = this.props;
    return items.map((item, index) => {
      const { ios, md } = item;
      return <ListGroupItem key={index} className="rounded-0 p-0">
        <ReactIconButton
          ios={ios}
          md={md}
          onlyIcon={onlyIcon}
          fullWidth={fullWidth}
          extraIconClass="mr-1"
        />
      </ListGroupItem>;
    });
  };
  render() {
    const { title, onlyIcon = false } = this.props;
    return (
      <Card className="reactMenu rounded-0 bg-dark" style={cardStyle(onlyIcon)}>
        {title ? <CardHeader>{title}</CardHeader> : ""}
        <CardBody className="p-0">
          <ListGroup className="rounded-0" flush>
            {onlyIcon ? this.generateIcons() : this.generateList()}
          </ListGroup>
        </CardBody>
      </Card>
    );
  }
}
