import React, { Component } from 'react';
import ReactListGroup from './ReactListGroup';

export default class ReactMenu extends Component {
  render() {
    const { listItems, onlyIcon } = this.props;
    return (
      listItems.map((listItem, index) =>
        <ReactListGroup key={index} {...listItem} fullWidth={true} onlyIcon={onlyIcon} />)
    );
  }
}
