import React from 'react';
import { FormGroup, Label } from 'reactstrap';

export default class ReactSelect extends React.Component {
  onSelectChange = (selected) => {
    const { onChange } = this.props;
    if (onChange) {
      onChange(selected);
    }
  }
  renderOptions = () => {
    const { options, value } = this.props;
    console.log("options");
    console.log(options);
    return options.map((option, index) => {
      return <option key={index} value={option} selected={value && option === value}>{option}</option>;
    })
  }
  render() {
    const { placeholder, label = "Label:" } = this.props;
    return (
      <FormGroup className="my-0">
        <Label className="px-2 my-0 text-light">{label}</Label>
        <select
          onChange = {this.onSelectChange}
        >
          <option>{placeholder}</option>
          {
            this.renderOptions()
          }
        </select>
      </FormGroup>
    );
  }
}
