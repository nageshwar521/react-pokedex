import React, { Component } from 'react';
import cn from 'classnames';
import './styles.css';
import { Card, Button, CardHeader, CardFooter, CardBody,
  CardTitle, CardText } from 'reactstrap';
import ReactIconButton from '../ReactIconButton';

const iconStyle = () => {
  return {
    position: "absolute",
    top: 0,
    right: 0,
    zIndex: 2
  };
};

const sidebarStyle = (props) => {
  const { position, open } = props;
  return {
    width: "250px"
  }
};

class ReactSidebar extends Component {
  constructor() {
    super();
    this.state = {
      open: false
    };
  }
  onCloseClick = () => {
    const { onCloseClick } = this.props;
    if (onCloseClick) {
      onCloseClick();
    }
  }
  render() {
    const { children, sidebarHeader, open } = this.props;
    // const childrenWithProps = React.Children.map(children, child =>
    //     React.cloneElement(child));
    const baseSidebarClass = "reactSidebar bg-dark text-light";
    const sidebarClass = open ? cn(baseSidebarClass, "slideIn") : cn(baseSidebarClass, "slideOut");

    return (
      <div className={sidebarClass} style={sidebarStyle(this.props, this.state)}>
        <ReactIconButton
          btnClass="shadow-0 outline-0"
          md={"close"}
          color="white"
          hoverColor="black"
          style={iconStyle()}
          onIconClick={this.onCloseClick} />
        <div>
          {children}
        </div>
      </div>
    );
  }
}

export default ReactSidebar;
