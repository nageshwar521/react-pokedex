import React from 'react';
import {
  Panel
} from 'react-bootstrap';

const panelStyle = {
  paddingLeft: "20px",
  paddingRight: "20px",
  height: "250px",
  width: "250px"
};

const imgStyle = {
  maxHeight: "220px",
  maxWidth: "200px"
};

export default class PokePanel extends React.Component {
  getPanelImage() {
    const { url, name } = this.props;
    const pokemonId = url.substring(34, url.length - 1);
    return <img
      style={imgStyle}
      src={`http://img.pokemondb.net/artwork/${name}.jpg`}
    />;
  }
  render() {
    const { name, xsCount = "" } = this.props;
    return (
      <div className="col-sm-4 col-md-3 col-lg-3 pokePanel_Container">
        <Panel style={panelStyle}>
          {this.getPanelImage()}
          <Panel.Body>
            <Panel.Title center>{name}</Panel.Title>
          </Panel.Body>
        </Panel>
      </div>
    );
  }
}
