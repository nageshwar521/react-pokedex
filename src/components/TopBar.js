import React from 'react';
import {
  Container,
  Collapse,
  Navbar,
  NavbarToggler,
  NavbarBrand,
  Nav,
  NavItem,
  NavLink,
  UncontrolledDropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem
} from 'reactstrap';
import ReactMenu from './common/ReactMenu';
import ReactSidebar from './common/ReactSidebar';
import ReactDropdown from './common/ReactSelect';
import ReactIconButton from './common/ReactIconButton';

export default class TopBar extends React.Component {
  constructor(props) {
    super(props);

    this.toggle = this.toggle.bind(this);
    this.state = {
      isOpen: false
    };
  }
  toggle() {
    this.setState({
      isOpen: !this.state.isOpen
    });
  }
  onMenuIconClick = () => {
    this.setState({
      onlyIcon: false
    });
  }
  render() {
    const { pokemonTypes } = this.props;
    return (
      <div className="w-100 topbar">
        <Navbar color="primary" dark expand="md">
          <Container className="px-3">
            <NavbarBrand href="/">Pokedex</NavbarBrand>
            <NavbarToggler onClick={this.toggle} />
            <Collapse isOpen={this.state.isOpen} navbar>
              <Nav className="ml-auto" navbar>
                <ReactDropdown label="Filter:" options={pokemonTypes} placeholder="All Types" />
              </Nav>
            </Collapse>
          </Container>
        </Navbar>
      </div>
    );
  }
}
