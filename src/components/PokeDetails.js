import React, { Component } from 'react';
import { ListGroup, ListGroupItem, Button, Badge } from 'reactstrap';

export default class PokeDetails extends Component {
  render() {
    const { pokemonId } = this.props;
    return (
      <div className="text-center">
        <Button color="danger" outline>
          Pokemon Id <Badge color="light">{pokemonId}</Badge>
        </Button>
      </div>
    );
  }
}
