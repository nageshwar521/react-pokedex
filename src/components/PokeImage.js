import React from 'react';
import classnames from 'classnames';

const PokeImage = ({ name, imgClass = "" }) => {
  // const pokemonId = url.substring(34, url.length - 1);
  return <img
    className={imgClass}
    width="200px"
    height="240px"
    src={`http://img.pokemondb.net/artwork/${name}.jpg`}
  />;
}

export default PokeImage;
