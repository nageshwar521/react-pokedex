import React, { Component } from 'react';
import { Col } from 'reactstrap';
import PokeImage from './PokeImage';
import ReactIconButton from './common/ReactIconButton';
import cn from 'classnames';

class PokeCard extends Component {
  onPokeDetailClick = () => {
    const { url, onPokeDetailClick } = this.props;
    if (onPokeDetailClick) {
      console.log("onPokeDetailClick");
      onPokeDetailClick(url);
    }
  }
  render() {
    const { colClass = "", name, url } = this.props;
    const columnClass = cn("px-0", colClass);
    return (
      <Col xs="3" className={columnClass}>
        <div className="card shadow rounded-0 m-3">
          <ReactIconButton
            fontSize="24px"
            color="#007bff"
            md="information-circle"
            btnPaddingClass="p-0"
            btnRoundClass="rounded-circle"
            btnPositionClass="position-absolute position-right"
            onIconClick={this.onPokeDetailClick}
          />
          <PokeImage name={name} imgClass="p-3 m-auto" />
          <div className="card-body text-center bg-primary text-light p-3">{name}</div>
        </div>
      </Col>
    );
  }
};

export default PokeCard;
