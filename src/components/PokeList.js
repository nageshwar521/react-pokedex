import React, { Component } from 'react';
import PokeCard from './PokeCard';
import ReactSidebar from './common/ReactSidebar';
import PokeDetails from './PokeDetails';
import {
  Container,
  Row,
  Card,
  CardHeader
} from 'reactstrap';

class PokeList extends Component {
  constructor() {
    super();
    this.state = {
      open: false,
      pokemonId: null
    };
  }
  onPokeDetailClick = (url) => {
    const pokemonId = url.substring(34, url.length - 1);
    this.setState({
      open: true,
      pokemonId
    });
  };
  onSidebarClose = () => {
    this.setState({
      open: false,
      pokemonId: null
    });
  };
  render() {
    const { pokemonData } = this.props;
    const { open, pokemonId } = this.state;
    return (
      <Container fluid className="p-0">
        <Row>
          {pokemonData.map(
            (pokeData, index) =>
              <PokeCard
                key={index}
                {...pokeData}
                onPokeDetailClick={this.onPokeDetailClick}
              />
          )}
        </Row>
        <Row>
          <ReactSidebar
            position="right"
            open={open}
            onCloseClick={this.onSidebarClose}
          >
            <PokeDetails pokemonId={pokemonId} />
          </ReactSidebar>
        </Row>
      </Container>
    );
  }
}

export default PokeList;
