import React from 'react';
import { connect } from 'react-redux';
import {
  fetchPokemonData,
  fetchPokemonTypes
} from '../actions';
import { SyncLoader } from 'react-spinners';
import {
  Container,
  Row,
  Col
} from 'reactstrap';
import PokeList from './PokeList';
import TopBar from './TopBar';
import ReactMenu from './common/ReactMenu';
import ReactSidebar from './common/ReactSidebar';

let listItems = [
  {
    items: [
      { md: "home", text: "item1" },
      { md: "home", text: "item1" },
    ]
  }
];

class Home extends React.Component {
  componentWillMount() {
    this.props.fetchPokemonData(100);
    this.props.fetchPokemonTypes();
  }
  render() {
    const { pokemonData = [], pokemonTypes = [], isFetching } = this.props;
    return (
      <Container fluid>
        <Row>
          <TopBar listItems={listItems} pokemonTypes={pokemonTypes} />
        </Row>
        <Container>
          <Row className="pokemon_container">
              {
                isFetching ?
                  <h1><SyncLoader color={"#424B49"} loading={true} /></h1> :
                  <PokeList pokemonData={pokemonData} />
              }
          </Row>
        </Container>
      </Container>
    );
  }
}

const mapStateToProps = (state) => {
  return {...state};
};

const mapDispatchToProps = (dispatch) => {
  return {
    fetchPokemonData: (limit) => {
      dispatch(fetchPokemonData(limit));
    },
    fetchPokemonTypes: () => {
      dispatch(fetchPokemonTypes());
    }
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(Home);
