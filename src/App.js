import React, { Component } from 'react';
import { Provider } from 'react-redux';
import './App.css';
import Home from './components/Home';
import store from './store/createStore';

class App extends Component {
  render() {
    return (
      <Provider store={store}>
        <Home />
      </Provider>
    );
  }
}

export default App;
