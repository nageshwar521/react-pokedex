import ActionTypes from '../constants';
import { filterPokemonTypes } from '../utils';
const POKEMON_API = `https://pokeapi.co/api/v2/`;

export const fetchPokemonData = (limit) => {
  return async dispatch => {
    dispatch({ type: ActionTypes.FETCH_POKEMON_DATA });
    await fetch(`${POKEMON_API}pokemon/?limit=${limit}`)
      .then(res => res.json())
      .then(response => {
        const data = response.results;
        dispatch({
          type: ActionTypes.FETCH_POKEMON_DATA_SUCCESS,
          data
        });
      })
      .then(error => {
        dispatch({
          type: ActionTypes.FETCH_POKEMON_DATA_FAILURE,
          error
        });
      });
  }
}

export const fetchPokemonTypes = () => {
  return async dispatch => {
    dispatch({ type: ActionTypes.FETCH_POKEMON_TYPES });
    await fetch(`${POKEMON_API}type`)
      .then(res => res.json())
      .then(response => {
        const data = filterPokemonTypes(response.results);
        dispatch({
          type: ActionTypes.FETCH_POKEMON_TYPES_SUCCESS,
          data
        });
      })
      .then(error => {
        dispatch({
          type: ActionTypes.FETCH_POKEMON_TYPES_FAILURE,
          error
        });
      });
  }
}

export default {
  fetchPokemonData,
  fetchPokemonTypes
}
