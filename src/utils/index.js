export const filterPokemonTypes = (pokemonData) => {
  return pokemonData.map(pokemon => pokemon.name);
}

export default {
  filterPokemonTypes
}
